# zadarma GLPI plugin

Проверяет баланс и статусы SIP пиров.

В случае если баланс становиться ниже минимального значения (можно данное
значение указать в настройках) сождается заявка о не обходимости пополнить баланс.

В случае если какой-л. из SIP пиров уходит в оффлайн также создаётся заявка с
информацией какой пир в оффлайне.

Внимание! Задачи планировщика выполняются только если у вас настроен запуск cron.php
из командной строки, например
```
* * * * * glpi php /home/www/glpi/public_html/front/cron.php
```

## Contributing

* Open a ticket for each bug/feature so it can be discussed
* Follow [development guidelines](http://glpi-developer-documentation.readthedocs.io/en/latest/plugins/index.html)
* Refer to [GitFlow](http://git-flow.readthedocs.io/) process for branching
* Work on a new branch on your own fork
* Open a PR that will be reviewed by a developer
