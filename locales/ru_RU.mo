��          �      \      �     �     �  )   �  +        F  
   V     a     |     �     �     �     �     �     �                  7   (     `  �  �  #        =  J   ]  U   �  '   �     &  ,   >  =   k  #   �  /   �  +   �  !   )  6   K     �     �     �      �  N   �  "   /                                                 
                       	                           Check SIP status Check balance Check the PBX settings or SIP settings %s Click on the link %s and form a bill to pay Global settings Install %s Installed / not configured Low account balance: %s %s Minimum balance Please activate %s Please install %s Plugin options This plugin requires %s Ticket owner Zadarma Zadarma API keys Zadarma config [%s] Low balance on the personal account Zadarma: %s %s [%s] The SIP peer %s is offline Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-21 11:14+0500
PO-Revision-Date: 2018-03-21 11:59+0500
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Проверить статус SIP Проверить баланс Проверьте настройки PBX или настройки SIP %s Нажмите ссылку %s и сформируйте счет для оплаты Глобальные настройки Установить %s Установлен / не настроен Низкий баланс лицевого счёта:  %s %s Минимальный баланс Пожалуйста, активируйте %s Пожалуста, установите %s Настройки плагина Для этого плагина требуется %s Владелец заявки Zadarma Ключи Zadarma API Конфигурация Zadarma [%s] Низкий баланс на лицевом счете "Zadarma": %s %s [%s] SIP пир %s вне сети 