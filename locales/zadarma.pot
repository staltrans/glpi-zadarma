# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-21 11:14+0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: setup.php:74 setup.php:82
#, php-format
msgid "This plugin requires %s"
msgstr ""

#: setup.php:78 setup.php:86
#, php-format
msgid "Please activate %s"
msgstr ""

#: setup.php:92
#, php-format
msgid "Please install %s"
msgstr ""

#: setup.php:120
msgid "Installed / not configured"
msgstr ""

#: setup.php:127
#, php-format
msgid "Install %s"
msgstr ""

#: inc/apikey.class.php:49 inc/profile.class.php:48
msgid "Zadarma API keys"
msgstr ""

#: inc/cron.class.php:39
msgid "Check balance"
msgstr ""

#: inc/cron.class.php:41
msgid "Check SIP status"
msgstr ""

#: inc/cron.class.php:71
#, php-format
msgid "[%s] Low balance on the personal account Zadarma: %s %s"
msgstr ""

#: inc/cron.class.php:72
#, php-format
msgid "Click on the link %s and form a bill to pay"
msgstr ""

#: inc/cron.class.php:92
#, php-format
msgid "Low account balance: %s %s"
msgstr ""

#: inc/cron.class.php:124
#, php-format
msgid "[%s] The SIP peer %s is offline"
msgstr ""

#: inc/cron.class.php:125
#, php-format
msgid "Check the PBX settings or SIP settings %s"
msgstr ""

#: inc/profile.class.php:34 inc/config.class.php:60
msgid "Zadarma"
msgstr ""

#: inc/profile.class.php:43 inc/profile.class.php:83
msgid "Plugin options"
msgstr ""

#: inc/config.class.php:44
msgid "Zadarma config"
msgstr ""

#: inc/config.class.php:88 inc/config.class.php:117
msgid "Global settings"
msgstr ""

#: inc/config.class.php:120
msgid "Minimum balance"
msgstr ""

#: inc/config.class.php:125
msgid "Ticket owner"
msgstr ""
