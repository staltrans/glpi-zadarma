<?php
/*
 -------------------------------------------------------------------------
 zadarma plugin for GLPI
 Copyright (C) 2017 by the zadarma Development Team.

 https://bitbucket.org/staltrans/zadarma
 -------------------------------------------------------------------------

 LICENSE

 This file is part of zadarma.

 zadarma is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 zadarma is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with zadarma. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_zadarma_install() {

   global $DB;

   $taskopt = [
      'allowmode' => CronTask::MODE_EXTERNAL,
      'mode'      => CronTask::MODE_EXTERNAL,
   ];

   CronTask::Register('PluginZadarmaCron', 'CheckBalance', 3 * MINUTE_TIMESTAMP, $taskopt);
   CronTask::Register('PluginZadarmaCron', 'CheckSipStatus', 137 * MINUTE_TIMESTAMP, $taskopt);

   $t_apikeys = 'glpi_plugin_zadarma_apikeys';

   if (!$DB->tableExists($t_apikeys)) {
      $query = "CREATE TABLE `$t_apikeys` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `entities_id` int(11) NOT NULL DEFAULT '0',
                  `is_recursive` tinyint(1) NOT NULL DEFAULT '0',
                  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL,
                  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `min_balance` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `tickets_id` int(11) NOT NULL DEFAULT '0',
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `key` (`key`),
                  KEY `name` (`name`),
                  KEY `entities_id` (`entities_id`),
                  KEY `is_recursive` (`is_recursive`),
                  KEY `is_deleted` (`is_deleted`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if ($DB->tableExists($t_apikeys)) {
      $query = "ALTER TABLE `$t_apikeys` ADD `tickets_id` int(11) DEFAULT NULL";
      $DB->query($query);

      $query = "ALTER TABLE `$t_apikeys` MODIFY `tickets_id` int(11) NOT NULL DEFAULT '0'";
      $DB->query($query);
   }

   if (class_exists('PluginZadarmaConfig') && class_exists('PluginZadarmaApikey')) {
      $config = new PluginZadarmaConfig();
      $keys = (array) $config->get('auth_keys');
      if (!empty($keys)) {
         $apikey = new PluginZadarmaApikey();
         $apikey->add($keys);
         $config->del('auth_keys');
      }
   }

   return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_zadarma_uninstall() {

   global $DB;

   $tables = [
      'glpi_plugin_zadarma_apikeys',
   ];

   foreach ($tables as $table) {
      if ($DB->tableExists($table)) {
          $query = "DROP TABLE `$table`";
          $DB->queryOrDie($query);
      }
   }

   CronTask::Unregister('CheckBalance');
   CronTask::Unregister('CheckSipStatus');

   $config = new PluginZadarmaConfig();
   $config->delAll();
   return true;

}
