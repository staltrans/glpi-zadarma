<?php

/**
 -------------------------------------------------------------------------
 zadarma plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/zadarma
 -------------------------------------------------------------------------

 LICENSE

 This file is part of zadarma.

 zadarma is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 zadarma is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with zadarma. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginZadarmaApi {

   const STATUS_OK  = 'success';
   const STATUS_ERR = 'error';

   protected $client;

   function __construct($key, $secret) {
      $this->client = new \Zadarma_API\Client($key, $secret);
   }

   function infoBalance() {
      return $this->call('/v1/info/balance/');
   }

   function infoPrice($number, $caller_id = null) {
      return $this->call('/v1/info/price/', [
         'number' => $number,
         'caller_id' => $caller_id
      ]);
   }

   function infoTimezone() {
      return $this->call('/v1/info/timezone/');
   }

   function tariff() {
      return $this->call('/v1/tariff/');
   }

   function requestCallback($from, $to, $sip = null, $predicted = null) {
      return $this->call('/v1/request/callback/', [
         'from' => $from,
         'to' => $to,
         'sip' => $sip,
         'predicted' => $predicted
      ]);
   }

   function sip() {
      return $this->call('/v1/sip/');
   }

   function sipStatus($number) {
      return $this->call("/v1/sip/$number/status/");
   }

   function sipCallerid($sip_id, $caller_id) {
      $opt = [
         'id' => $sip_id,
         'number' => $caller_id
      ];
      return $this->call('/v1/sip/callerid/', $opt, 'put');
   }

   function sipRedirection($id = null) {
      return $this->call('/v1/sip/redirection/', ['id' => $id]);
   }

   function directNumbers() {
      return $this->call('/v1/direct_numbers/');
   }

   function sim() {
      return $this->call('/v1/sim/');
   }

   /*
   function sipRedirection($sip_id, $status) {
      $opt = [
         'id' => $sip_id,
         'status' => $status
      ];
      return $this->call('/v1/sip/redirection/', $opt, 'put');
   }

   function sipRedirection($sip_id, $type, $number) {
      $opt = [
         'id' => $sip_id,
         'type' => $type,
         'number' => $number
      ];
      return $this->call('/v1/sip/redirection/', $opt, 'put');
   }
   */

   function pbxInternal() {
      return $this->call('/v1/pbx/internal/');
   }

   function pbxInternalStatus($pbxsip) {
      return $this->call("/v1/pbx/internal/$pbxsip/status/");
   }

   function pbxInternalRecording($id, $status, $email = null) {
      $opt = [
         'id' => $id,
         'status' => $status,
         'email' => $email
      ];
      return $this->call('/v1/pbx/internal/recording/', $opt, 'put');
   }

   function pbxRecordRequest($call_id, $pbx_call_id, $lifetime) {
      return $this->call('/v1/pbx/record/request/', [
         'call_id' => $call_id,
         'pbx_call_id' => $pbx_call_id,
         'lifetime' => $lifetime
      ]);
   }

   function smsSend($number, $meesage, $caller_id = null) {
      $opt = [
         'number' => $number,
         'message' => $message,
         'caller_id' => $caller_id
      ];
      return $this->call('/v1/sms/send/', $opt, 'post');
   }

   function statistic($start, $end, $sip = null, $cost_only = null, $type = null) {
      return $this->call('/v1/statistics/', [
         'start' => $start,
         'end' => $end,
         'sip' => $sip,
         'cost_only' => $cost_only,
         'type' => $type
      ]);
   }

   function statisticsPbx($start, $end, $version = 2) {
      return $this->call('/v1/statistics/pbx/', [
         'start' => $start,
         'end' => $end,
         'version' => $version
      ]);
   }

   function statisticsCallbackWidget($start, $end, $widget_id = null) {
      return $this->call('/v1/statistics/callback_widget/', [
         'start' => $start,
         'end' => $end,
         'widget_id' => $widget_id
      ]);
   }

   protected function call($method, $params = [], $requestType = 'get') {
      $answer = $this->client->call($method, $params, $requestType);
      $answer_obj = json_decode($answer);
      if ($answer_obj->status == self::STATUS_OK) {
         return $answer_obj;
      }
      $this->log($answer_obj->message, 1);
      return false;
   }

   protected function log($event, $level = 3) {
      Event::log(-1, 'plugin', $level, 'zadarma api', $event);
   }

}
