<?php

/**
 -------------------------------------------------------------------------
 zadarma plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/zadarma
 -------------------------------------------------------------------------

 LICENSE

 This file is part of zadarma.

 zadarma is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 zadarma is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with zadarma. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginZadarmaApikey extends CommonDBTM {

   // From CommonDBTM
   public $dohistory         = true;
   public $history_blacklist = ['date_mod'];

   static $rightname = 'plugin_zadarma_apikey';

   static function getTypeName($nb = 0) {
      return PluginZadarmaTr::__('Zadarma');
   }

   static function getAdditionalMenuLinks() {
      return ['config' => PluginZadarmaConfig::getFormURL()];
   }

   function rawSearchOptions() {

      $tab = [];

      $tab[] = [
         'id'   => 'common',
         'name' => PluginZadarmaTr::__('API ключи для Zadarma')
      ];

      $i = 1;

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'name',
         'name' => __('Name'),
         'datatype' => 'itemlink',
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'key',
         'name' => __('Key'),
         'massiveaction' => false,
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'secret',
         'name' => PluginZadarmaTr::__('Secret'),
         'massiveaction' => false,
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'min_balance',
         'name' => PluginZadarmaTr::__('Минимальный баланс'),
      ];

      return $tab;
   }

   function defineTabs($options = []) {

      $ong = [];
      $this->addDefaultFormTab($ong);
      $this->addStandardTab('Log', $ong, $options);
      return $ong;

   }

   function prepareInputForAdd($input) {
      if (!empty($input['min_balance']) && !is_numeric($input['min_balance'])) {
         $input['min_balance'] = '';
      }

      return $input;
   }

   function prepareInputForUpdate($input) {
      if (!empty($input['min_balance']) && !is_numeric($input['min_balance'])) {
         $input['min_balance'] = '';
      }

      return $input;
   }

   function showForm($ID, $options = []) {

      $this->initForm($ID, $options);
      $this->showFormHeader($options);
      echo '<tr>';
      echo '<td>' . __('Name') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'name');
      echo '</td>';
      echo '<td>' . __('Key') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'key');
      echo '</td>';
      echo'</tr>';
      echo '<tr>';
      echo '<td>' . PluginZadarmaTr::__('Минимальный баланс') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'min_balance');
      echo '</td>';
      echo '<td>' . PluginZadarmaTr::__('Secret') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'secret');
      echo '</td>';
      echo'</tr>';
      $this->showFormButtons($options);

      return true;
   }

   function save($post = []) {
      if (isset($post['add'])) {
         $this->check(-1, CREATE, $post);
         $this->add($post);
      } else if (isset($post['delete'])) {
         $this->check($post['id'], DELETE);
         $this->delete($post);
         $this->redirectToList();
      } else if (isset($post['restore'])) {
         $this->check($post['id'], PURGE);
         $this->restore($post);
         $this->redirectToList();
      } else if (isset($post['purge'])) {
         $this->check($post['id'], PURGE);
         $this->delete($post, 1);
         $this->redirectToList();
      } else if (isset($post['update'])) {
         $this->check($post['id'], UPDATE);
         $this->update($post);
      }

   }

}
