<?php

/**
 -------------------------------------------------------------------------
 zadarma plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/zadarma
 -------------------------------------------------------------------------

 LICENSE

 This file is part of zadarma.

 zadarma is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 zadarma is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with zadarma. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginZadarmaConfig extends PluginConfigVariable {

   const VAR_SETTINGS = 'settings';
   const PLUGIN       = 'zadarma';

   //static $rightname      = 'plugin_zadarma_config';
   protected $displaylist = false;

   static function getTypeName($nb = 0) {
      return PluginZadarmaTr::__('Zadarma config');
   }

   static function getMenuName() {
      return PluginZadarmaTr::__('Zadarma');
   }

   function defaultValue($variable) {
      switch ($variable) {
         case self::VAR_SETTINGS:
            return (object) [
               'ticket_owner' => 2,
               'min_balance'  => 0
            ];
      }
   }

   function showFormMain() {
      if (empty($this->fields['value'])) {
         $val = $this->defaultValue(self::VAR_SETTINGS);
      } else {
         $val = $this->fields['value'];
      }

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginZadarmaTr::__('Minimum balance') . '</td>';
      echo '<td>';
      echo '<input type="hidden" name="name" value="' . self::VAR_SETTINGS . '">';
      echo '<input type="hidden" name="plugin" value="' .  self::PLUGIN . '">';
      echo '<input type="text" name="value[min_balance]" size="60" value="' . $val->min_balance . '">';
      echo '</td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginZadarmaTr::__('Ticket owner') . '</td>';
      echo '<td>';
      User::dropdown([
         'value' => $val->ticket_owner,
         'right' => 'all',
         'name' => 'value[ticket_owner]'
      ]);
      echo '</td>';
      echo '</tr>';

   }

}
