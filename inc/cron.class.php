<?php

/**
 -------------------------------------------------------------------------
 zadarma plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/zadarma
 -------------------------------------------------------------------------

 LICENSE

 This file is part of zadarma.

 zadarma is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 zadarma is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with zadarma. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginZadarmaCron {

   static function getTypeName($nb = 0) {
      return PluginZadarmaTr::__('Задания модуля Zadarma');
   }

   static function cronInfo($task = null) {
      switch ($task) {
         case 'CheckBalance':
            return ['description' => PluginZadarmaTr::__('Проверка баланса')];
         case 'CheckSipStatus':
            return ['description' => PluginZadarmaTr::__('Проверка статуса SIP пиров')];
      }
      return [];
   }

   static function cronCheckBalance($task = null) {

      $apikey = new PluginZadarmaApikey();
      $config = new PluginZadarmaConfig();

      $opt = $config->get($config::VAR_SETTINGS);

      $condition = "`key` IS NOT NULL AND `key` <> '' AND `secret` IS NOT NULL AND `secret` <> '' AND `is_deleted` = 0";
      $keylist = $apikey->find($condition);
      foreach ($keylist as $key) {
         $api = new PluginZadarmaApi($key['key'], $key['secret']);
         try {
            $balance = $api->infoBalance();
         } catch (Exception $e) {
            $balance = null;
         }
         if (!empty($balance)) {
            if (!empty($key['min_balance']) && is_numeric($key['min_balance'])) {
               $min_balance = $key['min_balance'];
            } else {
               $min_balance = $opt->min_balance;
            }
            if ($balance->balance <= $min_balance) {
               if (empty($key['tickets_id'])) {
                  $ticket = new Ticket();
                  $fields = [
                     'name' => sprintf(PluginZadarmaTr::__('[%s] Низкий баланс на аккаунте Zadarma: %s %s'), $key['name'], $balance->balance, $balance->currency),
                     'content' => sprintf(PluginZadarmaTr::__('Для пополнения баланса перейдите по ссылке %s'), 'https://my.zadarma.com/pay/'),
                     '_users_id_requester' => $opt->ticket_owner
                  ];
                  if ($ticket->add($fields)) {
                     $key['tickets_id'] = $ticket->fields['id'];
                     $apikey->update($key);
                  }
               } else {
                  $user = new User();
                  $user->getFromDB($opt->ticket_owner);
                  $session = 0;
                  if (isset($_SESSION['glpiactive_entity'])) {
                     $session = $_SESSION['glpiactive_entity'];
                  }
                  $user->loadMinimalSession($session, true);
                  $ticket = new Ticket();
                  $ticket->getFromDB($key['tickets_id']);
                  if (in_array($ticket->fields['status'], [Ticket::CLOSED, Ticket::SOLVED])) {
                     $ticket->fields['status'] = Ticket::ASSIGNED;
                     $ticket->update($ticket->fields);
                  }
                  if ($ticket->isDeleted()) {
                     $ticket->restore($ticket->fields);
                  }
                  $followup = new TicketFollowup();
                  $followup->add([
                     'tickets_id' => $key['tickets_id'],
                     'content' => sprintf(PluginZadarmaTr::__('Мало средств на счёте: %s %s'), $balance->balance, $balance->currency)
                  ]);
               }
            } else {
               if (!empty($key['tickets_id'])) {
                  $key['tickets_id'] = 0;
                  $apikey->update($key);
               }
            }
         }
      }
      return 1;
   }

   static function cronCheckSipStatus($task = null) {

      $apikey = new PluginZadarmaApikey();
      $config = new PluginZadarmaConfig();

      $opt = $config->get($config::VAR_SETTINGS);

      $condition = "`key` IS NOT NULL AND `key` <> '' AND `secret` IS NOT NULL AND `secret` <> '' AND `is_deleted` = 0";
      $keylist = $apikey->find($condition);
      foreach ($keylist as $key) {
         $api = new PluginZadarmaApi($key['key'], $key['secret']);
         if ($sip = $api->sip()) {
            foreach ($sip->sips as $item) {
               try {
                  if ($status = $api->sipStatus($item->id)) {
                     if (!$status->is_online) {
                        $ticket = new Ticket();
                        $fields = [
                           'name' => sprintf(PluginZadarmaTr::__('[%s] SIP пир %s в офлайне'), $key['name'], $status->sip),
                           'content' => sprintf(PluginZadarmaTr::__('Проверьте наcтройки PBX или SIP пира %s'), 'https://my.zadarma.com/mysip/'),
                           '_users_id_requester' => $opt->ticket_owner
                        ];
                        $ticket->add($fields);
                     }
                  }
               } catch (Exception $e) {
                  continue;
               }
            }
         }
      }
      return 1;
   }

}
