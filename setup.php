<?php
/*
 -------------------------------------------------------------------------
 zadarma plugin for GLPI
 Copyright (C) 2017 by the zadarma Development Team.

 https://bitbucket.org/staltrans/zadarma
 -------------------------------------------------------------------------

 LICENSE

 This file is part of zadarma.

 zadarma is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 zadarma is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with zadarma. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_ZADARMA_VERSION', '0.2');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_zadarma() {
   global $PLUGIN_HOOKS;

   Plugin::registerClass('PluginZadarmaProfile', ['addtabon' => 'Profile']);

   $PLUGIN_HOOKS['menu_toadd']['zadarma'] = ['plugins' => 'PluginZadarmaApikey'];

   $PLUGIN_HOOKS['csrf_compliant']['zadarma'] = true;
}


/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_zadarma() {
   return [
      'name'           => 'zadarma',
      'version'        => PLUGIN_ZADARMA_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-zadarma',
      'minGlpiVersion' => '9.3'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_zadarma_check_prerequisites() {
   $plugin = new Plugin();
   if (!$plugin->isInstalled('config')) {
      printf(__('This plugin requires %s', 'zadarma'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('config')) {
      printf(__('Please activate %s', 'zadarma'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isInstalled('libraries')) {
      printf(__('This plugin requires %s', 'zadarma'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('libraries')) {
      printf(__('Please activate %s', 'zadarma'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!class_exists('\Zadarma_API\Client')) {
      global $CFG_GLPI;
      $url = $CFG_GLPI['root_doc'] . '/plugins/libraries/front/list.php';
      printf(__('Please install %s', 'zadarma'), '<a href="' . $url . '">zadarma/user-api-v1</a>');
      return false;
   }
   // Strict version check (could be less strict, or could allow various version)
   if (version_compare(GLPI_VERSION, '9.3', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.3');
      } else {
         echo "This plugin requires GLPI >= 9.3";
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_zadarma_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'zadarma');
   }
   return false;
}

function plugin_zadarma_libraries_composer_help() {
   return [
      'title' => sprintf(__('Install %s', 'zadarma'), 'zadarma/user-api-v1'),
      'content' => '[... how to install zadarma/user-api-v1 ...]'
   ];
}
